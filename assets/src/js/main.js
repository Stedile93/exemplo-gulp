$(document).ready(function() {
    $('#header').sticky({topSpacing:0, zIndex:1});

    $('.navbar-toggle').on('click', function() {
        $('#header').toggleClass('menu-open');
    });

    $('.filter-cases:not(.filter-cases--mobile)').on('click', function(e) {
        if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 768) {
            e.preventDefault();
            $(this).toggleClass('menu-open');
            $('.filter-cases--mobile').slideToggle();
        }
    });

    $('ul.social-nav li a').attr('target', '_blank');

    $('.clients-carousel').slick({
        speed: 3000,
        autoplay: true,
        autoplaySpeed: 0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        dots: false,
        arrows: false,
        infinite: true
    });

    $("form :input").focus(function() {
        $("label[for='" + this.id + "']").addClass("focus");
    }).blur(function() {
        if ($("#" + this.id).val() == '') {
            $("label[for='" + this.id + "']").removeClass("focus");
        }
    });

    $('.page-contact select option:first').html($('.page-contact input#text-first-option').val());

    // Mask
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };

    var spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('#inputPhone').mask(SPMaskBehavior, spOptions);
    $('#inputPhoneQuote').mask(SPMaskBehavior, spOptions);

    // Validate
    if ($('.wpcf7-form--contact').length || $('.wpcf7-form--quote').length) {
        var bootstrapValidateOptions = {
            ignore: '',
            errorElement: 'span',
            errorClass: 'wpcf7-not-valid-tip'
        }
    }

    if ($('.wpcf7-form--contact').length) {
        $('.wpcf7-form--contact').validate(bootstrapValidateOptions);

        $('#inputEmail').rules("add", {
            email: true
        });
    }

    if ($('.wpcf7-form--quote').length) {
        $('.wpcf7-form--quote').validate(bootstrapValidateOptions);

        $('#inputEmailQuote').rules("add", {
            email: true
        });
    }

    // Página de Cases (Admin Área)
    if ($('.admin-area-galery').length) {
        $('.admin-area-galery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
        });
    }
});