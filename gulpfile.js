'use strict'; /* jshint ignore:line */

const gulp    = require('gulp')
    , path    = require('path')
    , fs      = require('fs')
    , pkg     = JSON.parse(fs.readFileSync('package.json'))
    , version = parseInt(pkg.version)
    , $       = require('gulp-load-plugins')();

var cleanCSS  = require('gulp-clean-css');


var banner = ['/**',
    ' * <%= pkg.author %> - <giu.stedile@gmail.com>',
    ' * <%= pkg.name %>',
    ' * @version v<%= pkg.version %>',
    ' * @date <%= Date().toString() %>',
    ' */',
    '\n\n'].join('\n');


//       _
//   ___| | ___  __ _ _ __
//  / __| |/ _ \/ _` | '_ \
// | (__| |  __/ (_| | | | |
//  \___|_|\___|\__,_|_| |_|

gulp.task('clean', function () {
    return gulp.src(['assets/css/*', 'assets/fonts/*', 'assets/js/*', 'assets/img/*'])
        .pipe($.clean());
        //.pipe($.notify({ message: 'Clean is ready!', onLast: true }));
});


// Coppy fonts
gulp.task('fonts', function() {
    return gulp.src('assets/src/fonts/**/*')
        .pipe(gulp.dest('assets/fonts'));
        //.pipe($.notify({ message: 'Fonts is ready!', onLast: true }));
});


//    _                                _       _
//   (_) __ ___   ____ _ ___  ___ _ __(_)_ __ | |_
//   | |/ _` \ \ / / _` / __|/ __| '__| | '_ \| __|
//   | | (_| |\ V / (_| \__ \ (__| |  | | |_) | |_
//  _/ |\__,_| \_/ \__,_|___/\___|_|  |_| .__/ \__|
// |__/                                 |_|

gulp.task('js', function () {
    return gulp.src(['assets/src/js/**/*.js'])
        .pipe($.concat('main.js'))
        .pipe($.header(banner, { pkg : pkg } ))
        .pipe(gulp.dest('assets/js/'))
        .pipe($.rename('main.min.js'))
        //.pipe($.uglify())
        .pipe(gulp.dest('assets/js/'));
        //.pipe($.notify({ message: 'JS is ready!', onLast: true }));
});


//      _         _
//  ___| |_ _   _| | ___  ___
// / __| __| | | | |/ _ \/ __|
// \__ \ |_| |_| | |  __/\__ \
// |___/\__|\__, |_|\___||___/
//          |___/

gulp.task('sass', function () {
    return gulp.src('assets/src/scss/**/*.scss')
        //.pipe($.debug())
        .pipe($.compass({
            css: __dirname +'/assets/css/',
            sass: __dirname +'/assets/src/scss/',
            image: __dirname +'/assets/src/img/',
            generated_images_path: __dirname +'/assets/img/',
            relative_assets: true
        }))
        .on('error', function (error) {
            console.log(error);
        })
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('assets/css/'));
        //.pipe($.notify({ message: 'CSS & Sprite is ready!', onLast: true }));
});


//  _
// (_)_ __ ___   __ _
// | | '_ ` _ \ / _` |
// | | | | | | | (_| |
// |_|_| |_| |_|\__, |
//              |___/

gulp.task('img', function () {
  return gulp.src('assets/src/img/*.{jpg,png,gif}')
    .pipe(gulp.dest('assets/img/'));
    //.pipe($.notify({ message: 'IMG is ready!', onLast: true }));
});


//                _       _
// __      ____ _| |_ ___| |__
// \ \ /\ / / _` | __/ __| '_ \
//  \ V  V / (_| | || (__| | | |
//   \_/\_/ \__,_|\__\___|_| |_|

gulp.task('watch', function () {
    gulp.watch(['assets/src/scss/**/*.scss', 'assets/src/img/sprite/*.png'], ['sass']);
    gulp.watch(['assets/src/js/**/*.js'], ['js']);
    gulp.watch(['assets/src/img/*.{jpg,png,gif}'], ['img']);
});


//  _           _ _     _
// | |__  _   _(_) | __| |
// | '_ \| | | | | |/ _` |
// | |_) | |_| | | | (_| |
// |_.__/ \__,_|_|_|\__,_|

gulp.task('build', ['js', 'fonts', 'sass', 'img']);


//      _       __             _ _
//   __| | ___ / _| __ _ _   _| | |_
//  / _` |/ _ \ |_ / _` | | | | | __|
// | (_| |  __/  _| (_| | |_| | | |_
//  \__,_|\___|_|  \__,_|\__,_|_|\__|

gulp.task('default', ['build', 'watch']);
